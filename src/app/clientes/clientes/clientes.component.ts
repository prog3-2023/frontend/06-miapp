import {Component, OnInit} from '@angular/core';
import {Cliente} from "../interfaces/cliente.interfaces";
import {ClientesService} from "../services/clientes.service";

@Component({
  selector: 'app-clientes',
  templateUrl: './clientes.component.html',
  styleUrls: ['./clientes.component.css']
})
export class ClientesComponent implements OnInit {
  clientes?: Cliente[];

  constructor(private clientesService: ClientesService) {
  }

  ngOnInit(): void {
    this.getClientes();
  }

  private getClientes() {
    this.clientesService.getClientes().subscribe({
      next: (value) => {
        console.log(JSON.stringify(value));
        this.clientes = value.data;
      },
      error: (err) => {
        console.log(err.error.msg);
      }
    });
  }
}
