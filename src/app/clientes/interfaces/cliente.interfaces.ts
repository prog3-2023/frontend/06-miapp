export interface ClienteResponse {
  page:        number;
  per_page:    number;
  total:       number;
  total_pages: number;
  data:        Cliente[];
  support:     Support;
}

export interface Cliente {
  id:         number;
  email:      string;
  first_name: string;
  last_name:  string;
  avatar:     string;
}

export interface Support {
  url:  string;
  text: string;
}
