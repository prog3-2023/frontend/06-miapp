import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {ClienteResponse} from "../interfaces/cliente.interfaces";
import {Observable} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ClientesService {

  private url = 'https://reqres.in/api/users?page=1';

  constructor(private http: HttpClient) {
  }

  getClientes(): Observable<ClienteResponse> {
    return this.http.get<ClienteResponse>(this.url);
  }
}
