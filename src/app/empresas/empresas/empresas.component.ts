import {Component, Input, OnInit} from '@angular/core';
import {Empresa} from "../interfaces/empresa.interface";

@Component({
  selector: 'app-empresas',
  templateUrl: './empresas.component.html',
  styleUrls: ['./empresas.component.css']
})
export class EmpresasComponent implements OnInit {
  @Input() empresas?: Empresa[];

  constructor() {
  }

  ngOnInit(): void {
  }

}
