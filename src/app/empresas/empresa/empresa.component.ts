import {Component, OnInit} from '@angular/core';
import {Empresa} from "../interfaces/empresa.interface";

@Component({
  selector: 'app-empresa',
  templateUrl: './empresa.component.html',
  styleUrls: ['./empresa.component.css']
})
export class EmpresaComponent implements OnInit {

  empresas: Empresa[] = [
    {
      id: 1,
      nombre: 'Microsoft',
      cuit: '33112223336'
    },
    {
      id: 2,
      nombre: 'Apple',
      cuit: '44112223336'
    },
    {
      id: 3,
      nombre: 'Google',
      cuit: '55112223336'
    },
    {
      id: 4,
      nombre: 'Amazon',
      cuit: '55112223336'
    }
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

  agregarEmpresa(empresa: Empresa) {
    this.empresas.push(empresa);
  }
}
