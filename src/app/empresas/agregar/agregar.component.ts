import {Component, EventEmitter, OnInit, Output} from '@angular/core';
import {Empresa} from "../interfaces/empresa.interface";
import {EmpresaComponent} from "../empresa/empresa.component";

@Component({
  selector: 'app-agregar',
  templateUrl: './agregar.component.html',
  styleUrls: ['./agregar.component.css']
})
export class AgregarComponent implements OnInit {

  newEmpresa: Empresa = {
    nombre: '',
    cuit: ''
  };
  @Output() onNewEmpresa: EventEmitter<Empresa> = new EventEmitter<Empresa>();

  constructor() {
  }

  ngOnInit(): void {
  }

  confirmar() {
    this.onNewEmpresa.emit(this.newEmpresa);
    this.newEmpresa = {
      nombre: '',
      cuit: ''
    };
  }
}
