import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {EmpresasComponent} from "./empresas/empresas.component";
import {EmpresaComponent} from "./empresa/empresa.component";
import {FormsModule} from "@angular/forms";
import { AgregarComponent } from './agregar/agregar.component';



@NgModule({
  declarations: [
    EmpresasComponent,
    EmpresaComponent,
    AgregarComponent
  ],
  exports: [
    EmpresasComponent,
    EmpresaComponent
  ],
  imports: [
    CommonModule,
    FormsModule
  ]
})
export class EmpresasModule { }
